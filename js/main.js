let weeklyQuestTitles = document.querySelectorAll('[data-quest-title]');
for (let title of weeklyQuestTitles) {
   title.innerHTML = title.innerHTML.toUpperCase();
}

let weeklyQuestText = document.querySelectorAll(' [data-quest-text]');
for (let text of weeklyQuestText) {
    if (text.innerHTML.length >= 20) {
        text.innerHTML = text.innerHTML.slice(0, 20) + '...';
    }
}

// Открытие/закрытие вложенного меню

let menuDropDown= document.querySelectorAll('[data-menuDropDown]');

menuDropDown.forEach(function(block) {
    block.addEventListener('click', function() {
        const parent = block.parentNode;

        let menuDropDownList = parent.querySelector('.menu-drop-down-list');
        menuDropDownList.classList.toggle('menu-drop-down-show');

        let iconPointer = parent.querySelector('[data-pointer]');
        if (iconPointer.classList.contains('rotate-icon')) {
            removeClass(parent.querySelector('[data-pointer]'), 'rotate-icon');
        } else {
            addClass(parent.querySelector('[data-pointer]'), 'rotate-icon');
        }
    })
})

function addClass(element, cssClass) {
    element.classList.add(cssClass);
}

function removeClass(element, cssClass) {
    element.classList.remove(cssClass);
}

// Формирование нового блока "Все лекции"

let allCards = document.querySelectorAll('[data-group]');
let blockAllCards = document.getElementById('all-cards');
let lecturesSum = document.getElementById('lectures-sum');

allCards.forEach(card => {
    blockAllCards.innerHTML += card.outerHTML;
});

lecturesSum.innerHTML = allCards.length + ' лекций';

//Фильтрация

let filters = document.querySelectorAll('[data-filter]');

filters.forEach(function(filter) {
    filter.addEventListener('click', function() {

        const parent = blockAllCards.parentNode;
        let cards = parent.querySelectorAll('.weekly-quest-card');

        cards.forEach(card => {
            if (filter.dataset.filter === 'sum' || card.dataset.group === filter.dataset.filter) {
                card.style.display = '';
            } else {
                card.style.display = 'none';
            }
        })
    })
})

//Создание объекта
function getCardsObj (cards) {
    let cardsObj = {};

    cards.forEach(function (card) {
        let backgroundImg = card.querySelector('[data-quest-img]');
        let backgroundImgUrl = backgroundImg.style.backgroundImage.slice(5,-1).replace(/"/g, "");

        let cardObj = {
            "title": card.querySelector('[data-quest-title]').innerHTML,
            "description": card.querySelector('[data-quest-text]').innerHTML,
            "date": card.querySelector('[data-quest-date]').innerHTML,
            "image": backgroundImgUrl,
            "label": card.querySelector('[data-quest-label]').innerHTML
        }

        let dataGroup = card.dataset.group;

        if (!(dataGroup in cardsObj)) {
            cardsObj[dataGroup] = [];
        }

        cardsObj[dataGroup].push(cardObj);
    });

    console.log(cardsObj);
}

getCardsObj(allCards);
